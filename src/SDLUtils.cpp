#include "../include/SDLUtils.h"

SDL_Window* createWindow(const int sizeX, const int sizeY){
    SDL_Window* windows;
    SDL_Renderer* renderer;
    if ( SDL_CreateWindowAndRenderer( sizeX, sizeY, SDL_WINDOW_SHOWN,
                                      &windows, &renderer ) != 0 )
        throw InitError();
    return windows;
}
