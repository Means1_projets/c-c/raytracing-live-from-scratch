#include <iostream>
#include "../include/SDLUtils.h"
#include "../include/Entity.h"
#include "../include/Triangle.h"
#include "../include/Sphere.h"
#include <thread>
#include <math.h>
#include <cmath>
#include <time.h>
#include <SDL2/SDL_ttf.h>
#include <string>


/*
 * Lesson 1: Hello World!
 */

int sizeX = 500;
int sizeY = 500;
Vector3 player{0, 4, -4};
Entity* entity;

int max_index = 5;
int aliasing = 1;

const Vector3 light_at{0, 100, 0};
const Vector3 light_color{255,255,255};

// Transform model coordinates to world coordinates
void entityToWorld(std::vector<Entity*>& scene_objects, std::vector<Object*>& objectWorld){
	for (const auto& entity : scene_objects) {
		Mat4 mat4 = entity->generateModelMatrix();
		for (const auto& object : entity->scene_objects) {
			objectWorld.push_back(object->toWorld(mat4));
		}
	}
}

//Process one ray 
void processOneRay(std::vector<Object*>& objectWorld, Vector3& final_color, Vector3& ray_direction){
	
	
	Vector3 ray_origin = player;
	float distance;
	double reflectivity;
	double ray_energy_left = 1.f;
	Vector3 ray_hit_at;
	Vector3 ray_bounced_direction;

	for (int bounce = 0; bounce <= 100; ++bounce){

		Vector3 color = Vector3(0, 0, 0);
		
		float min_hit_distance{std::numeric_limits<float>::max()};
		
		Object* closest_object{nullptr};
		Vector3 normal;
		for (const auto& object : objectWorld) {
			if (object->is_hit_by_ray(ray_origin,
									ray_direction,
									ray_hit_at,
									ray_bounced_direction,
									distance, 
									normal)) {
				if (distance < min_hit_distance) {
					min_hit_distance = distance;
					closest_object = object;
				}
			}
		}

		bool hit = true;
		double diffuse_factor_at_hit;
		double hardness_at_hit;
		double specular_factor_at_hit;
		if (closest_object) {
			closest_object->is_hit_by_ray(ray_origin,
											ray_direction,
											ray_hit_at,
											ray_bounced_direction,
											distance,
											normal);
			ray_origin = ray_hit_at;
			ray_direction = ray_bounced_direction;
			color = closest_object->color;
			reflectivity = closest_object->reflectivity;
			diffuse_factor_at_hit = closest_object->diffuse_factor_at_hit;
			hardness_at_hit = closest_object->hardness_at_hit;
			specular_factor_at_hit = closest_object->specular_factor_at_hit;
		} else {
			if (ray_direction.y > 0) {
				color = Vector3(0.7, 0.6, 1)*255 * (std::pow(1-ray_direction.y, 2));
				reflectivity = 0.f;
			} else {
				const float distance = -ray_origin.y / ray_direction.y;
				const float x = ray_origin.x + ray_direction.x * distance;
				const float z = ray_origin.z + ray_direction.z * distance;
				ray_hit_at = Vector3(x, 0, z);
				if ((int)std::abs(std::floor(x)) % 2 == 0 && 
					(int)std::abs(std::floor(z)) % 2 == 0)
				{
					color = {0, 166, 125};
				} else if ((int)std::abs(std::floor(x)) % 2 == 1 && 
					(int)std::abs(std::floor(z)) % 2 == 1){
					color = {255, 166, 166};
				}else {
					color = {255, 255, 255};
				}
				reflectivity = 0.f;
				diffuse_factor_at_hit = 0.8;
				hardness_at_hit = 0;
				specular_factor_at_hit = 0;
            	normal = {0, 1, 0};
			}
		}

		

		if (closest_object || ray_direction.y <= 0){
			Vector3 a,b,d;
			float c;
			for (const auto& object : objectWorld) {
				if (object->is_hit_by_ray(ray_hit_at,
											(light_at - ray_hit_at).normalize(), 
											a,b,c,d)) {
					hit = false;
					
					break;
				}
			}

			if (hit) {
				const double ambient_light{0.3f};
				const double diffuse_light{std::max(0., normal.dot((light_at - ray_hit_at).normalize()))};
				const double specular_factor{std::max(0., (light_at - ray_hit_at).normalize().dot(ray_direction))};
				color = color * ambient_light +
						color * diffuse_light * diffuse_factor_at_hit +
						light_color * std::pow(specular_factor, hardness_at_hit) * specular_factor_at_hit;
			} else {
          		const float ambient_light{0.3f};
            	color = color * ambient_light;
          	}
		}
		final_color = final_color + (color*(ray_energy_left*(1-reflectivity)));
		ray_energy_left *= reflectivity;
		if (ray_energy_left <= 0)
			break;
	}
}

void renderText(SDL_Renderer* renderer, const int x, const int y, const char *text, TTF_Font *font){
	SDL_Rect rect;
	int text_width;
    int text_height;
    SDL_Surface *surface = TTF_RenderText_Solid(font, text, {255, 255, 255, 0});
	SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
	text_width = surface->w;
    text_height = surface->h;
    SDL_FreeSurface(surface);
    rect.x = x;
    rect.y = y;
    rect.w = text_width;
    rect.h = text_height;
	SDL_RenderCopy(renderer, texture, NULL, &rect);
    SDL_DestroyTexture(texture);
}

void taskDesplay(const int sizeX, const int sizeY, SDL_Renderer* renderer, SDL_Surface* surface){
 	TTF_Font *font = TTF_OpenFont("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf", 10);
	if (font == NULL) {
        fprintf(stderr, "error: font not found\n");
        exit(EXIT_FAILURE);
    }
	
 	std::vector<Entity*> scene_objects;
    
	entity = Entity::createCube();
	scene_objects.push_back(entity);

  
  	std::vector<Object*> object;
	Sphere* sphere = new Sphere(Vector3(0, 0, 0), 1, Vector3(0, 0, 255), 0.1);
	sphere->diffuse_factor_at_hit = 3;
	object.push_back(sphere);
	Entity* entity2 = new Entity(Vector3(2, 3, 10), Vector3(0, 0, 0), 1, object);
	scene_objects.push_back(entity2);

	int index = 0;

	std::vector<uint8_t> pixels(sizeX * sizeY * 4, 0);

	std::pair<double, double> aliasingd[9] {
		{0, 0}, {0.25, 0}, {-0.25, 0}, 
		{0, 0.25}, {0, -0.25}, {0.25, 0.25}, 
		{-0.25, -0.25}, {0.25, -0.25}, {-0.25, 0.25}}; 

    while ( true ) {
		const clock_t begin_time = clock();


		std::vector<Object*> objectWorld;

		entityToWorld(scene_objects, objectWorld);
	
		index = index % max_index;

		for (int y = sizeX / 2 ; y > - sizeX / 2; --y){
			for (int x = - sizeY / 2; x <= sizeY / 2; ++x){
				if (abs(x % max_index) != index){
					continue;
				}
				
				Vector3 AA_color{0, 0, 0};

				for (int sample = 0; sample < aliasing; ++sample) {
					Vector3 final_color{0, 0, 0};
					//static_cast<float>(rand()) / RAND_MAX
					auto& ddirection = aliasingd[sample];
					Vector3 ray_direction = Vector3(0.002*(x - 0.5 + ddirection.first),
											0.002*(y - 0.5 + ddirection.second),
											1).normalize();		
					
					processOneRay(objectWorld, final_color, ray_direction);
					
        			AA_color = AA_color + final_color*(1./aliasing);
				}
				int i = (x + sizeY / 2) * 4 + (sizeX / 2 - y) * (sizeX * 4);
				pixels[i] = static_cast<Uint8>(std::max(0., std::min(255., AA_color.z)));
				pixels[i + 1] = static_cast<Uint8>(std::max(0., std::min(255., AA_color.y)));
				pixels[i + 2] = static_cast<Uint8>(std::max(0., std::min(255., AA_color.x)));
				pixels[i + 3] = 255;
			}	
		}
		index = (index + 1) % max_index;

		SDL_LockSurface(surface);

		memcpy(surface->pixels, pixels.data(), surface->pitch * surface->h);
		
		SDL_UnlockSurface(surface);



		SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
		SDL_RenderCopy(renderer, texture, NULL, NULL);
		
		
		
		renderText(renderer, 0, 0, (std::to_string(int(CLOCKS_PER_SEC/float( clock () - begin_time))) + " FPS").c_str(), font);
		renderText(renderer, 0, 10, ("Skipping : " + std::to_string(max_index)).c_str(), font);
		renderText(renderer, 0, 20, ("Aliasing : " + std::to_string(aliasing)).c_str(), font);
		
		
		SDL_RenderPresent(renderer);
    
	}
}


int main(int, char**){
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	} 
	TTF_Init();
	SDL_Window* windows = SDL_CreateWindow("CPU raytracing C++/SDL2", 0, 0, sizeX, sizeY, SDL_WINDOW_SHOWN);
	
	SDL_Renderer* renderer = SDL_CreateRenderer(windows, -1, SDL_RENDERER_PRESENTVSYNC  | SDL_RENDERER_ACCELERATED);
    SDL_Surface* pSurface = SDL_GetWindowSurface(windows);
	SDL_Surface* surface = SDL_CreateRGBSurface(0, sizeX, sizeY, 32, 0, 0, 0, 0);

	//Main loop flag
	bool quit = false;

	
	std::thread t1([renderer, surface]() { taskDesplay( sizeX, sizeY, renderer, surface);});

	


	
	//Event handler
	SDL_Event e;

	//While application is running
	while( !quit )
	{
		//Handle events on queue
		while( SDL_PollEvent( &e ) != 0 )
		{
			//User requests quit
			if( e.type == SDL_QUIT )
			{
				quit = true;
			} else if (e.type == SDL_KEYDOWN ){
				switch (e.key.keysym.sym){
					case SDLK_p:
						if (max_index > 1){
							max_index--;
						}
						break;
					case SDLK_m:
							max_index++;
						break;
					case SDLK_o:
						if (aliasing > 1){
							aliasing--;
						}
						break;
					case SDLK_l:
						if (aliasing < 9)
							aliasing++;
						break;
					case SDLK_z:
						player = player + Vector3{0, 0, 0.1};
						break;
					case SDLK_s:
						player = player + Vector3{0, 0, -0.1};
						break;
					case SDLK_d:
						player = player + Vector3{0.1, 0, 0};
						break;
					case SDLK_q:
						player = player + Vector3{-0.1, 0, 0};
						break;
					case SDLK_LSHIFT:
						player = player + Vector3{0, -0.1, 0};
						break;
					case SDLK_SPACE:
						player = player + Vector3{0, 0.1, 0};
						break;
					case SDLK_KP_8:
						entity->rot = entity->rot + Vector3(0.03, 0, 0);
						break;
					case SDLK_KP_2:
						entity->rot = entity->rot + Vector3(-0.03, 0, 0);
						break;
					case SDLK_KP_4:
						entity->rot = entity->rot + Vector3(0, 0.03, 0);
						break;
					case SDLK_KP_6:
						entity->rot = entity->rot + Vector3(0, -0.03, 0);
						break;
					case SDLK_KP_7:
						entity->rot = entity->rot + Vector3(0, 0, 0.03);
						break;
					case SDLK_KP_9:
						entity->rot = entity->rot + Vector3(0, 0, -0.03);
						break;
					case SDLK_KP_1:
						entity->pos = entity->pos + Vector3(0, 0.1, 0);
						break;
					case SDLK_KP_3:
						entity->pos = entity->pos + Vector3(0, -0.1, 0);
						break;
				}
			}
		}
	}
	SDL_Quit();
	
	return 0;
}