#include "../include/Sphere.h"

Sphere::Sphere(const Vector3& center, const float radius, const Vector3& color, const float reflectivity) : 
  center{center}, radius{radius} { 
	this->color = color;
  this->reflectivity = reflectivity;
}

bool Sphere::is_hit_by_ray(const Vector3& incoming_ray_origin,
							const Vector3& incoming_ray_direction,
							Vector3& outgoing_ray_origin,
							Vector3& outgoing_ray_direction,
							float& distance,
              Vector3& out_normal) const
  {
    const Vector3 p = center - incoming_ray_origin;;
    const float dotp = p.dot(p);
    const float r_2 = radius * radius;
    const float threshold = std::sqrt(dotp - r_2);
    const float b = p.dot(incoming_ray_direction);

    if (b > threshold) {
      const float s = std::sqrt(dotp - b * b);
      const float t = std::sqrt(r_2 - s * s);
      distance = b - t;

      if (distance < 1e-3)
        return false;

      outgoing_ray_origin = incoming_ray_origin + incoming_ray_direction*distance;
      out_normal = (-p + incoming_ray_direction * distance).normalize();
      outgoing_ray_direction = (incoming_ray_direction + 
                                out_normal*(out_normal.dot(-incoming_ray_direction))*2).normalize();
      return true;
    }
    return false;
  }
  
Object* Sphere::toWorld(Mat4& matrixModel) const {
	Sphere* sphere = new Sphere(matrixModel.mul(this->center), this->radius, this->color, this->reflectivity);
  sphere->diffuse_factor_at_hit = this->diffuse_factor_at_hit;
	sphere->hardness_at_hit = this->hardness_at_hit;
	sphere->specular_factor_at_hit = this->specular_factor_at_hit;
	return sphere;
}

std::ostream& operator<<(std::ostream& out, const Sphere & e){
    out << "Sphere(center : " << e.center << ", radius : " << e.radius << ")";
    return out;
}

