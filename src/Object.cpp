#include "../include/Object.h"

Object::
  Object()
    : color(255, 255, 255),
      reflectivity(1),
		  diffuse_factor_at_hit(1),
		  specular_factor_at_hit(0),
		  hardness_at_hit(0)
  { }
