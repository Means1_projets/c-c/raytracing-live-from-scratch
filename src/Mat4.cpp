#include "../include/Mat4.h"


Mat4::Mat4(const double m11, const double m12, const double m13, const double m14,
        const double m21, const double m22, const double m23, const double m24,
        const double m31, const double m32, const double m33, const double m34) {
          this->mat[0][0] = m11;
          this->mat[0][1] = m12;
          this->mat[0][2] = m13;
          this->mat[0][3] = m14;
          this->mat[1][0] = m21;
          this->mat[1][1] = m22;
          this->mat[1][2] = m23;
          this->mat[1][3] = m24;
          this->mat[2][0] = m31;
          this->mat[2][1] = m32;
          this->mat[2][2] = m33;
          this->mat[2][3] = m34;
    }

Mat4::Mat4(const Mat4& mat){
    for (int i = 0; i < 3; i++){
      for (int j = 0; j < 4; j++){
        this->mat[i][j] = mat.mat[i][j];
      }
    }
}


Mat4::Mat4(){
    for (int i = 0; i < 3; i++){
      for (int j = 0; j < 4; j++){
        this->mat[i][j] = 0;
      }
    }
}


void Mat4::rotate(float angle, Vector3 axis){
    double c = (float)cos(angle);
    double s = (float)sin(angle);
    double oneminusc = 1.0F - c;
    double xy = axis.x * axis.y;
    double yz = axis.y * axis.z;
    double xz = axis.x * axis.z;
    double xs = axis.x * s;
    double ys = axis.y * s;
    double zs = axis.z * s;
    double f00 = axis.x * axis.x * oneminusc + c;
    double f01 = xy * oneminusc + zs;
    double f02 = xz * oneminusc - ys;
    double f10 = xy * oneminusc - zs;
    double f11 = axis.y * axis.y * oneminusc + c;
    double f12 = yz * oneminusc + xs;
    double f20 = xz * oneminusc + ys;
    double f21 = yz * oneminusc - xs;
    double f22 = axis.z * axis.z * oneminusc + c;
    double t00 = this->mat[0][0] * f00 + this->mat[0][1] * f01 + this->mat[0][2] * f02;
    double t01 = this->mat[1][0] * f00 + this->mat[1][1] * f01 + this->mat[1][2] * f02;
    double t02 = this->mat[2][0] * f00 + this->mat[2][1] * f01 + this->mat[2][2] * f02;
    double t10 = this->mat[0][0] * f10 + this->mat[0][1] * f11 + this->mat[0][2] * f12;
    double t11 = this->mat[1][0] * f10 + this->mat[1][1] * f11 + this->mat[1][2] * f12;
    double t12 = this->mat[2][0] * f10 + this->mat[2][1] * f11 + this->mat[2][2] * f12;
    this->mat[0][2] = this->mat[0][0] * f20 + this->mat[0][1] * f21 + this->mat[0][2] * f22;
    this->mat[1][2] = this->mat[1][0] * f20 + this->mat[1][1] * f21 + this->mat[1][2] * f22;
    this->mat[2][2] = this->mat[2][0] * f20 + this->mat[2][1] * f21 + this->mat[2][2] * f22;
    this->mat[0][0] = t00;
    this->mat[1][0] = t01;
    this->mat[2][0] = t02;
    this->mat[0][1] = t10;
    this->mat[1][1] = t11;
    this->mat[2][1] = t12;
}

void Mat4::scale(Vector3 vec){
    this->mat[0][0] = this->mat[0][0] * vec.x;
    this->mat[1][0] = this->mat[1][0] * vec.x;
    this->mat[2][0] = this->mat[2][0] * vec.x;
    this->mat[0][1] = this->mat[0][1] * vec.y;
    this->mat[1][1] = this->mat[1][1] * vec.y;
    this->mat[2][1] = this->mat[2][1] * vec.y;
    this->mat[0][2] = this->mat[0][2] * vec.z;
    this->mat[1][2] = this->mat[1][2] * vec.z;
    this->mat[2][2] = this->mat[2][2] * vec.z;
}


Vector3 Mat4::mul(Vector3 v){
  return Vector3(
    v.x*this->mat[0][0]+v.y*this->mat[0][1]+v.z*this->mat[0][2]+this->mat[0][3],
    v.x*this->mat[1][0]+v.y*this->mat[1][1]+v.z*this->mat[1][2]+this->mat[1][3],
    v.x*this->mat[2][0]+v.y*this->mat[2][1]+v.z*this->mat[2][2]+this->mat[2][3]);
  }


std::ostream& operator<<(std::ostream& out, const Mat4 & mat){
    out << "Mat4(" << std::endl;
    for (int i = 0; i < 3; i++){
      for (int j = 0; j < 4; j++){
        out << mat.mat[i][j] << " ";
      } 
      out << std::endl;
    }
    return out;
}
