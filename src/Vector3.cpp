#include "../include/Vector3.h"



Vector3::Vector3(){}
Vector3::Vector3(const double x, const double y, const double z) : x{x}, y{y}, z{z} {}

Vector3::Vector3(const Vector3 &other) : x{other.x}, y{other.y}, z{other.z} {}

double Vector3::length() const{
    return sqrt(this->x * this->x + this->y * this->y + this->z * this->z);
}

Vector3 Vector3::normalize() const{
    double l = length();
    if (l == 0)
        return Vector3(*this);
    return Vector3(this->x/l, this->y/l, this->z/l);
}

double Vector3::dot(const Vector3& vec) const{
    return this->x * vec.x + this->y * vec.y + this->z * vec.z;
}
Vector3 Vector3::cross(const Vector3& vec){
    return Vector3(this->y * vec.z - this->z * vec.y,
                this->z * vec.x - this->x * vec.z,
                this->x * vec.y - this->y * vec.x);
}

Vector3 operator+ (const Vector3& v1, const Vector3& v2){
    return Vector3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}

Vector3 operator- (const Vector3& v1, const Vector3& v2){
    return Vector3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}

Vector3 operator* (const Vector3& v1, const Vector3& v2){
    return Vector3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
}

Vector3 Vector3::operator* (const double& r) const {
    return Vector3(this->x * r, this->y * r, this->z * r);
}

Vector3 Vector3::operator-() const{
    return *this * -1;
}

std::ostream& operator<<(std::ostream& out, const Vector3 & v){
    out << "Vector3(" << v.x << ", " << v.y << ", " << v.z << ")";
    return out;
}
