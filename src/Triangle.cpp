#include "../include/Triangle.h"

Triangle::Triangle(const Vector3& p0, const Vector3& p1, const Vector3& p2, const Vector3& color, const double reflectivity) : 
  p0{p0}, p1{p1}, p2{p2} {
    this->u = p1 - p0;
    this->v = p2 - p0;
	this->color = color;
	this->reflectivity = reflectivity;
    this->normal = this->u.cross(this->v).normalize();
  }

bool Triangle::is_hit_by_ray(const Vector3& incoming_ray_origin,
							const Vector3& incoming_ray_direction,
							Vector3& outgoing_ray_origin,
							Vector3& outgoing_ray_direction,
							float& distance,
                            Vector3& out_normal) const
  {
    if (normal.dot(incoming_ray_direction) >= 0)
      return false;
		const double u_factor = (-(incoming_ray_origin.x - p0.x)*(incoming_ray_direction.y*v.z - incoming_ray_direction.z*v.y) + (incoming_ray_origin.y - p0.y)*(incoming_ray_direction.x*v.z - incoming_ray_direction.z*v.x) - (incoming_ray_origin.z - p0.z)*(incoming_ray_direction.x*v.y - incoming_ray_direction.y*v.x))/(incoming_ray_direction.x*u.y*v.z - incoming_ray_direction.x*u.z*v.y - incoming_ray_direction.y*u.x*v.z + incoming_ray_direction.y*u.z*v.x + incoming_ray_direction.z*u.x*v.y - incoming_ray_direction.z*u.y*v.x);
		const double v_factor = ((incoming_ray_origin.x - p0.x)*(incoming_ray_direction.y*u.z - incoming_ray_direction.z*u.y) - (incoming_ray_origin.y - p0.y)*(incoming_ray_direction.x*u.z - incoming_ray_direction.z*u.x) + (incoming_ray_origin.z - p0.z)*(incoming_ray_direction.x*u.y - incoming_ray_direction.y*u.x))/(incoming_ray_direction.x*u.y*v.z - incoming_ray_direction.x*u.z*v.y - incoming_ray_direction.y*u.x*v.z + incoming_ray_direction.y*u.z*v.x + incoming_ray_direction.z*u.x*v.y - incoming_ray_direction.z*u.y*v.x);
		const double ray_factor = (-(incoming_ray_origin.x - p0.x)*(u.y*v.z - u.z*v.y) + (incoming_ray_origin.y - p0.y)*(u.x*v.z - u.z*v.x) - (incoming_ray_origin.z - p0.z)*(u.x*v.y - u.y*v.x))/(incoming_ray_direction.x*u.y*v.z - incoming_ray_direction.x*u.z*v.y - incoming_ray_direction.y*u.x*v.z + incoming_ray_direction.y*u.z*v.x + incoming_ray_direction.z*u.x*v.y - incoming_ray_direction.z*u.y*v.x);

		if (u_factor < 0 or u_factor > 1 or
				v_factor < 0 or v_factor > 1 or
				u_factor+v_factor > 1 or
				ray_factor < 0)
			return false;
    
		distance = (incoming_ray_direction*ray_factor).length();

    if (distance < 1e-3)
      return false;

		out_normal = normal;
		outgoing_ray_origin = p0 + u*u_factor + v*v_factor;
		outgoing_ray_direction = (incoming_ray_direction + normal*(normal.dot(-incoming_ray_direction))*2).normalize();
		return true;
  }
  
Object* Triangle::toWorld(Mat4& matrixModel) const {
	Triangle* triangle = new Triangle(matrixModel.mul(this->p0), matrixModel.mul(this->p1), matrixModel.mul(this->p2), this->color, this->reflectivity);
	triangle->diffuse_factor_at_hit = this->diffuse_factor_at_hit;
	triangle->hardness_at_hit = this->hardness_at_hit;
	triangle->specular_factor_at_hit = this->specular_factor_at_hit;
	return triangle;
}

std::ostream& operator<<(std::ostream& out, const Triangle & e){
    out << "Triangle(p0 : " << e.p0 << ", p1 : " << e.p1 << ", p2 : " << e.p2 << ", u : " << e.u << ", v : " << e.v << ", normal : " << e.normal << ")";
    return out;
}

