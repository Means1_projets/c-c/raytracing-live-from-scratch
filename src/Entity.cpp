#include "../include/Entity.h"

Entity::Entity(const Vector3& pos, const Vector3& rot, const double& scale, const std::vector<Object*> scene_objects) :
  pos{pos}, rot{rot}, scale{scale}, scene_objects{scene_objects} {}


Mat4 Entity::generateModelMatrix(){
  Mat4 mat4 = Mat4(); 
  mat4.mat[0][0] = 1; 
  mat4.mat[1][1] = 1; 
  mat4.mat[2][2] = 1; 
  mat4.mat[0][3] = pos.x;
  mat4.mat[1][3] = pos.y;
  mat4.mat[2][3] = pos.z;
  mat4.rotate(rot.x, Vector3(1, 0, 0));
  mat4.rotate(rot.y, Vector3(0, 1, 0));
  mat4.rotate(rot.z, Vector3(0, 0, 1));
  return mat4;
}

Entity* Entity::createCube(){
  std::vector<Object*> object;
	object.push_back(new Triangle(Vector3(-1, -1, 1), Vector3(1, -1, 1), Vector3(-1, 1, 1), Vector3(255, 0, 0), 0.5));
	object.push_back(new Triangle(Vector3(-1, 1, 1), Vector3(1, -1, 1), Vector3(1, 1, 1), Vector3(255, 0, 0), 0.5));
	object.push_back(new Triangle(Vector3(-1, 1, 1), Vector3(1, 1, 1), Vector3(-1, 1, -1), Vector3(255, 0, 0), 0.5));
	object.push_back(new Triangle(Vector3(-1, 1, -1), Vector3(1, 1, 1), Vector3(1, 1, -1), Vector3(255, 0, 0), 0.5));
	object.push_back(new Triangle(Vector3(-1, 1, -1), Vector3(1, 1, -1), Vector3(-1, -1, -1), Vector3(255, 0, 0), 0.5));
	object.push_back(new Triangle(Vector3(-1, -1, -1), Vector3(1, 1, -1), Vector3(1, -1, -1), Vector3(255, 0, 0), 0.5));
	object.push_back(new Triangle(Vector3(-1, -1, -1), Vector3(1, -1, -1), Vector3(-1, -1, 1), Vector3(255, 0, 0), 0.5));
	object.push_back(new Triangle(Vector3(-1, -1, 1), Vector3(1, -1, -1), Vector3(1, -1, 1), Vector3(255, 0, 0), 0.5));
	object.push_back(new Triangle(Vector3(1, -1, 1), Vector3(1, -1, -1), Vector3(1, 1, 1), Vector3(255, 0, 0), 0.5));
	object.push_back(new Triangle(Vector3(1, 1, 1), Vector3(1, -1, -1), Vector3(1, 1, -1), Vector3(255, 0, 0), 0.5));
	object.push_back(new Triangle(Vector3(-1, -1, -1), Vector3(-1, -1, 1), Vector3(-1, 1, -1), Vector3(255, 0, 0), 0.5));
	object.push_back(new Triangle(Vector3(-1, 1, -1), Vector3(-1, -1, 1), Vector3(-1, 1, 1), Vector3(255, 0, 0), 0.5));

	return new Entity(Vector3(0, 1, 3), Vector3(0, 0, 0), 2, object);

}


std::ostream& operator<<(std::ostream& out, const Entity & e){
    out << "Entity(pos : " << e.pos << ", rot : " << e.rot << ", s : " << e.scale << ")";
    return out;
}
