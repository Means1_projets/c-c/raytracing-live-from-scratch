#ifndef MAT4_H
#define MAT4_H
#include <iostream>
#include <tgmath.h>
#include "Vector3.h"

class Mat4
{
private:
    /* data */
public:

    double mat[3][4];
    Mat4(const double m11, const double m12, const double m13, const double m14,
        const double m21, const double m22, const double m23, const double m24,
        const double m31, const double m32, const double m33, const double m34);
    Mat4(const Mat4& mat4);
    Mat4();
    Vector3 mul(Vector3 v);
    void rotate(float angle, Vector3 axis);
    void scale(Vector3 vec);
    friend std::ostream& operator<<(std::ostream& out, const Mat4 & mat);

};  

#endif
