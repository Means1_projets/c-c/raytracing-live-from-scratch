#ifndef ENTITY_H
#define ENTITY_H
#include "Vector3.h"
#include "vector"
#include "Object.h"
#include "Mat4.h"
#include "Triangle.h"

class Entity
{
    public:
        Vector3 pos {};
        Vector3 rot {};
        double scale {};  
        std::vector<Object*> scene_objects {};


        Entity(const Vector3& pos, const Vector3& rot, const double& scale, const std::vector<Object*> scene_objects);


        Mat4 generateModelMatrix();

        friend std::ostream& operator<<(std::ostream& out, const Entity & e);
    

        static Entity* createCube();
};

#endif