#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "Object.h"
#include "Vector3.h"
#include "Mat4.h"

class Triangle : public Object
{
    public:
        Vector3 p0, p1, p2, u, v, normal;
    
        Triangle(const Vector3& p0, const Vector3& p1, const Vector3& p2, const Vector3& color, const double reflectivity);

        bool is_hit_by_ray(const Vector3& incoming_ray_origin,
                     const Vector3& incoming_ray_direction,
                     Vector3& outgoing_ray_origin,
                     Vector3& outgoing_ray_direction,
                     float& distance,
                     Vector3& out_normal) const;

        Object* toWorld(Mat4& matrixModel) const;
        friend std::ostream& operator<<(std::ostream& out, const Triangle & e);
};

#endif