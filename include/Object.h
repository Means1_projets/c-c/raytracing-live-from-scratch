#ifndef OBJECT_H
#define OBJECT_H
#include "Vector3.h"
#include "Mat4.h"

class Object
{
    public:
        Vector3 color;
        float reflectivity;
		double diffuse_factor_at_hit;
		double specular_factor_at_hit;
		double hardness_at_hit;
        Object();

    virtual bool is_hit_by_ray(const Vector3& incoming_ray_origin,
                             const Vector3& incoming_ray_direction,
                             Vector3& outgoing_ray_origin,
                             Vector3& outgoing_ray_direction,
                             float& distance,
                             Vector3& out_normal) const = 0;

    virtual Object* toWorld(Mat4& matrixModel) const = 0;
    
};

#endif