#ifndef VECTOR3_H
#define VECTOR3_H
#include <iostream>
#include <tgmath.h>


class Vector3
{
private:
    /* data */
public:

    double x {};
    double y {};
    double z {};
    Vector3();
    Vector3(const double x, const double y, const double z);
    Vector3(const Vector3 &other);

    double length() const;
    Vector3 normalize() const;
    double dot(const Vector3& vec) const;
    Vector3 cross(const Vector3& vec);


    friend Vector3 operator+ (const Vector3& v1, const Vector3& v2);
    friend Vector3 operator- (const Vector3& v1, const Vector3& v2);
    friend Vector3 operator* (const Vector3& v1, const Vector3& v2);
    Vector3 operator* (const double& r) const;
    Vector3 operator-() const;
    friend std::ostream& operator<<(std::ostream& out, const Vector3 & v);

};  

#endif
