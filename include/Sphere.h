#ifndef SPHERE_H
#define SPHERE_H
#include "Object.h"
#include "Vector3.h"
#include "Mat4.h"

class Sphere : public Object
{
    public:
        Vector3 center;
        float radius;
    
        Sphere(const Vector3& center, const float radius, const Vector3& color, const float reflectivity);

        bool is_hit_by_ray(const Vector3& incoming_ray_origin,
                     const Vector3& incoming_ray_direction,
                     Vector3& outgoing_ray_origin,
                     Vector3& outgoing_ray_direction,
                     float& distance,
                     Vector3& out_normal) const;

        Object* toWorld(Mat4& matrixModel) const;
        friend std::ostream& operator<<(std::ostream& out, const Sphere & e);
};

#endif